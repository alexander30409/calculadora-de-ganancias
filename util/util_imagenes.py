
import tkinter as tk
from PIL import Image, ImageTk
from tkinter import PhotoImage

def leer_imagen(path, size):
    return ImageTk.PhotoImage(Image.open(path).resize(size, Image.ADAPTIVE))
