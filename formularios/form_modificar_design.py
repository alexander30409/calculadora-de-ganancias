import tkinter as tk
from tkinter import ttk
from tkinter import font
from config import  COLOR_CUERPO_PRINCIPAL,COLOR_MENU_LATERAL
from bd.conexion import BSD,InsertingBSD,ModificarBSD,ListaBSD,EliminarBSD,FilaBSD

class FormularioModificarBD():
        
    def __init__(self, panel_principal, logo=None):
        # Crear paneles: barra superior
        self.barra_superior = tk.Frame(panel_principal)
        self.barra_superior.pack(side=tk.TOP, fill=tk.X, expand=False) 

        # Crear paneles: barra inferior
        self.barra_inferior = tk.Frame(panel_principal)
        self.barra_inferior.pack(side=tk.BOTTOM, fill='both', expand=True)
        #Titulo barra superior
        self.labelTitulo=tk.Label(self.barra_superior,text='Tabla Ingredientes')  
        self.labelTitulo.config(fg='#222d33',font=('Roboto',30))
        self.labelTitulo.pack(side=tk.TOP,fill="both",expand=True)
        self.construirWidgets()
        self.construirID()
        self.Tabla()
    def construirWidgets(self):
        #BOTONES     
        altura=260
        font_awesome = font.Font(family="FontAwesome", size=15)
            #INSERTAR 
        self.btnINSERTAR= tk.Button(self.barra_inferior, text="Insertar", font=font_awesome,command=self.ComandoInsertar, bg=COLOR_MENU_LATERAL, 
                                 fg=COLOR_CUERPO_PRINCIPAL)
        self.btnINSERTAR.place(x=70, y=altura, width=100, height=40)
            #CAMBIAR
        self.btnCAMBIAR= tk.Button(self.barra_inferior, text="Cambiar", font=font_awesome,command=self.ComandoModificar, bg=COLOR_MENU_LATERAL, 
                                 fg=COLOR_CUERPO_PRINCIPAL)
        self.btnCAMBIAR.place(x=180, y=altura, width=100, height=40)
            #ELIMINAR
        self.btnELIMINAR= tk.Button(self.barra_inferior, text="Eliminar", font=font_awesome,command=self.ComandoEliminar, bg=COLOR_MENU_LATERAL, 
                                 fg=COLOR_CUERPO_PRINCIPAL)
        self.btnELIMINAR.place(x=290, y=altura, width=100, height=40)
            #Limpiar
        self.btnLIMPIAR= tk.Button(self.barra_inferior, text="Limpiar", font=font_awesome,command=self.ComandoLimpiar, bg=COLOR_MENU_LATERAL, 
                                 fg=COLOR_CUERPO_PRINCIPAL)
        self.btnLIMPIAR.place(x=400, y=altura, width=100, height=40)

        #Entradas de Texto
        validatecommand = self.barra_inferior.register(lambda char: char not in ' :;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvwxyz{|}~!"#$%&*+,)(/-')
        #nombre
        self.lnombre=tk.Label(self.barra_inferior,text='Nombre:',font=('Roboto',15))  
        self.lnombre.place(x=70,y=altura+100)

        self.Cnombre=tk.Entry(self.barra_inferior,font=font_awesome)
        self.Cnombre.place(x=70, y=altura+125,width=120)
            #cantidad
        self.lcant=tk.Label(self.barra_inferior,text='Cantidad:',font=('Roboto',15))  
        self.lcant.place(x=220,y=altura+100)

        self.Ccant=tk.Entry(self.barra_inferior,font=font_awesome,validate='key',validatecommand=(validatecommand,'%S'))
        self.Ccant.place(x=220, y=altura+125,width=120)
            #Precio Costo
        self.lprecioC=tk.Label(self.barra_inferior,text='Precio Costo:',font=('Roboto',15))  
        self.lprecioC.place(x=370,y=altura+100)

        self.CprecioC=tk.Entry(self.barra_inferior,font=font_awesome,validate='key',validatecommand=(validatecommand,'%S'))
        self.CprecioC.place(x=370, y=altura+125,width=120)

    def construirID(self):
        altura=260
        #ID
        self.l_id=tk.Label(self.barra_inferior,text='ID:',font=('Roboto',15))  
        self.l_id.place(x=70,y=altura+65)

        self.IDs=ttk.Combobox(self.barra_inferior,state='readonly',values=ListaBSD('ingredientes','id'),width=5,height=8)
        self.IDs.place(x=100,y=altura+70)
        def selection_changed(event):
            seleccion=self.IDs.get()
            self.ComandoLimpiar()
            self.Cnombre.insert(0,FilaBSD('ingredientes',seleccion)[1])
            self.Ccant.insert(0,FilaBSD('ingredientes',seleccion)[2])
            self.CprecioC.insert(0,FilaBSD('ingredientes',seleccion)[3])
        self.IDs.bind("<<ComboboxSelected>>", selection_changed)


    def Tabla(self):
        self.tabla = ttk.Treeview(self.barra_inferior, columns=("#1","#2","#3",'#4'),height=10)
        self.tabla.column("#0",width=150,anchor='center')
        self.tabla.column("#1",width=150,anchor='center')
        self.tabla.column("#2",width=150,anchor='center')
        self.tabla.column("#3",width=140,anchor='center')
        self.tabla.column("#4",width=140,anchor='center')
        self.tabla.heading("#0", text="ID")
        self.tabla.heading("#1", text="Nombre")
        self.tabla.heading("#2", text="Cantidad(Gr)")
        self.tabla.heading("#3",text="Precio Costo")
        self.tabla.heading("#4", text="Precio Unitario")
        for dato in BSD('ingredientes'):
            self.tabla.insert(
                "",tk.END,
                text=dato[0],
                values=dato[1:],
            )
        self.tabla.place(x=10,y=20)
        # Agrega una barra de desplazamiento
        scrollbar = ttk.Scrollbar(self.barra_inferior, orient=tk.VERTICAL, command=self.tabla.yview)
        self.tabla.configure(yscrollcommand=scrollbar.set)
        scrollbar.place(x=741,y=20,height=225)
        def handle_click(event):
            if self.tabla.identify_region(event.x, event.y) == "separator":
                return "break"

        self.tabla.bind('<Button-1>', handle_click)
#COMANDO DE BOTONES
    def ComandoInsertar(self):
        nombre=self.Cnombre.get()
        Cantidad=int(float(self.Ccant.get())*1000)
        PrecioCosto=float(self.CprecioC.get())
        PrecioUnitario=PrecioCosto/Cantidad
        if nombre not in ListaBSD('ingredientes','nombre'):
            datos=(nombre,Cantidad,PrecioCosto,PrecioUnitario)
            InsertingBSD(datos)
            self.Tabla()
            self.construirID()
    def ComandoModificar(self):
        ID=int(self.IDs.get())
        nombre=self.Cnombre.get()
        Cantidad=int(float(self.Ccant.get())*1000)
        PrecioCosto=float(self.CprecioC.get())
        PrecioUnitario=PrecioCosto/Cantidad
        if self.Cnombre.get() not in ListaBSD('ingredientes','nombre') or nombre==FilaBSD('ingredientes',ID)[1]:
            datos=(nombre,Cantidad,PrecioCosto,PrecioUnitario,ID)
            ModificarBSD(datos)
            self.Tabla()
    def ComandoEliminar(self):
        datos=(int(self.IDs.get()))
        EliminarBSD(datos)
        self.Tabla()
        self.construirID()
    def ComandoLimpiar(self):#fallos tecnicos
        self.Cnombre.delete(0,tk.END)
        self.Ccant.delete(0,tk.END)
        self.CprecioC.delete(0,tk.END)
        