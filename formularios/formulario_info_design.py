import tkinter as tk
from tkinter import ttk
from tkinter import font
from config import  COLOR_CUERPO_PRINCIPAL,COLOR_MENU_LATERAL

class FormularioInfoDesign():

    def __init__(self, panel_principal):
        self.barra_superior=tk.Frame(panel_principal)
        self.barra_superior.pack(side=tk.TOP, fill="both")
        
        self.labelTitulo=tk.Label(self.barra_superior,text='Informacion')  
        self.labelTitulo.config(fg='#222d33',font=('Georgia',30))
        self.labelTitulo.pack(side=tk.TOP,fill="both",expand=True)

        # Crear paneles: barra inferior
        self.barra_inferior = tk.Frame(panel_principal)
        self.barra_inferior.pack(side=tk.BOTTOM, fill='both', expand=True)

        self.textos()

    def textos(self):
        self.labelVersion = tk.Label(self.barra_inferior, text="Version 1.0")
        self.labelVersion.config(fg="black", font=("Georgia", 15), pady= 10, width=20, justify="left")
        self.labelVersion.grid(row=1,column=1)

        self.labelAutores = tk.Label(self.barra_inferior, text="Autores")
        self.labelAutores.config(fg="black", font=("Georgia", 15), pady= 10, width=20, justify="left")
        self.labelAutores.grid(row=2,column=1)

        self.labelAutor1 = tk.Label(self.barra_inferior, text="Orlando Parra")
        self.labelAutor1.config(fg="black", font=("Georgia", 15), pady= 10, width=20, justify="left")
        self.labelAutor1.grid(row=3,column=1)

        self.labelAutor2 = tk.Label(self.barra_inferior, text="Sandra Sepulveda")
        self.labelAutor2.config(fg="black", font=("Georgia", 15), pady= 10, width=20, justify="left")
        self.labelAutor2.grid(row=4,column=1)

        self.labelAutor3 = tk.Label(self.barra_inferior, text="Alexander Rodriguez")
        self.labelAutor3.config(fg="black", font=("Georgia", 15), pady= 10, width=20, justify="left")
        self.labelAutor3.grid(row=5,column=1)
        #for i in range(6):
            