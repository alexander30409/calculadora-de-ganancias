import tkinter as tk
from tkinter import ttk, messagebox
from tkinter import font
from config import *
from bd.conexion import *


class FormularioIndice(tk.Tk):
    def __init__(self, panel_principal, logo=None):
        self.barra_superior=tk.Frame(panel_principal)
        self.barra_superior.pack(side=tk.TOP, fill="both")
        
        self.labelTitulo=tk.Label(self.barra_superior,text='Indice')  
        self.labelTitulo.config(fg='#222d33',font=('Georgia',30))
        self.labelTitulo.pack(side=tk.TOP,fill="both",expand=True)

        # Crear paneles: barra inferior
        self.barra_inferior = tk.Frame(panel_principal)
        self.barra_inferior.pack(side=tk.BOTTOM, fill='both', expand=True)

        self.indice()
        self.lista_combos()
        self.mostrar()

    def indice(self):
        self.titulo=tk.Label(self.barra_inferior, text="Combos")
        self.titulo.config(fg="black", font=("Georgia", 15), pady= 10, width=20, justify="left")
        self.titulo.grid(row=1,column=1)
        
        #titulo datos
        self.datos=tk.Label(self.barra_inferior, text='Datos Combo')
        self.datos.config(fg='black', font=('Georgia', 15), pady=5,padx=10, width=20, justify='left')
        self.datos.grid(row=1,column=3)
        
        #caja para ubicar los datos 
        self.datos_caja=tk.Frame(self.barra_inferior)
        self.datos_caja.config(width=200,height=250)
        self.datos_caja.grid(row=2,column=3, padx=30, pady=10,rowspan=4)
        #boton limpiar pantalla
        self.limpiar=tk.Button(self.barra_inferior, text="Limpiar", command=self.clear,font=("FontAwesome",12),bg=COLOR_MENU_LATERAL,fg=COLOR_CUERPO_PRINCIPAL,width=23)
        self.limpiar.grid(row=4,column=1,sticky='N')
    def clear(self):
        for widget in self.datos_caja.winfo_children():
            widget.destroy()
        self.v=0
        self.lista.clear()


    def lista_combos(self):
        #lista de los combos
        self.list_combos=tk.Listbox(self.barra_inferior,width=35,height=20)
        self.list_combos.grid(row=2,column=1, sticky='N')
        
        #boton de mostrar
        self.boton_show=tk.Button(self.barra_inferior, text='Mostrar', command=self.mostrar,font=("FontAwesome",12),bg=COLOR_MENU_LATERAL,fg=COLOR_CUERPO_PRINCIPAL,width=23)
        self.boton_show.grid(row=3,column=1, sticky='N')

        for i in ListaBSD('combos','nombre'):
            self.list_combos.insert(tk.END,i)
        self.v=0
        self.lista=[]
        
    def mostrar(self):
        if self.v>2:
            self.mensaje=messagebox.showinfo(title="debe limpiar la pantalla",message="Solo puede mostrar hasta 2 combos") 
            return 
        if len(self.list_combos.curselection())!=0 and self.list_combos.curselection() not in self.lista:
            self.lista.append(self.list_combos.curselection())
            
            
            #llamar a la funcion crear frame
            self.marco_datos()


            #valores
            self.dato=tk.Label(self.caja_combo, text=self.list_combos.get(self.list_combos.curselection()))
            self.dato.config(bg=COLOR_MENU_CURSOR_ENCIMA)
            self.dato.grid(row=2,column=0)
            self.v+=1

    #crea los frames de cada combo
    def marco_datos(self):
        ID=ID_BSD('combos',self.list_combos.get(self.list_combos.curselection()))
        #caja de los datos
        fila=FilaBSD('combos',ID)
        self.caja_combo=tk.Frame(self.datos_caja)
        self.caja_combo.config(bg=COLOR_CUERPO_PRINCIPAL, width=250, height=250)
        self.caja_combo.pack()
        self.caja_combo.columnconfigure(0,weight=20)

        #textos fijos
        self.nombre=tk.Label(self.caja_combo, text='Nombre Combo')
        self.nombre.config(bg=COLOR_CUERPO_PRINCIPAL)
        self.nombre.grid(row=0,column=0)

        self.texto1=tk.Label(self.caja_combo, text="Ingredientes ")
        self.texto1.config(bg=COLOR_CUERPO_PRINCIPAL)
        self.texto1.grid(row=0,column=1)
        #+str(fila[3])
        total=fila[3]/(1-fila[2]/100)
        self.cant=tk.Label(self.caja_combo, text="Cantidad (Gr/ml) ")
        self.cant.config(bg=COLOR_CUERPO_PRINCIPAL)
        self.cant.grid(row=0,column=2)

        self.subt=tk.Label(self.caja_combo, text="Subtotal ($): "+str(fila[3])[0:6])
        self.subt.config(bg="#B5B9BE")
        self.subt.grid(row=3,column=0)

        self.total=tk.Label(self.caja_combo, text="Total ($): "+str(total)[0:6])
        self.total.config(bg="#B5B9BE")
        self.total.grid(row=4,column=0)

        self.ganancia=tk.Label(self.caja_combo, text="Ganancia ($): "+str(total*fila[3]/100)[0:6])
        self.ganancia.config(bg="#B5B9BE",)
        self.ganancia.grid(row=5,column=0)
        self.ingredients()
    def ingredients(self):
        ID=ID_BSD('combos',self.list_combos.get(self.list_combos.curselection()))
        self.ingr=Listaingredientesdelcombo(ID)
        for i in range(len(self.ingr)):
            self.elemtns=tk.Label(self.caja_combo, text=self.ingr[i][0],font=("FontAwesome",6),bg=COLOR_CUERPO_PRINCIPAL,)
            self.elemtns.grid(row=i+1,column=1,sticky='n',padx=10,)
            self.elemtns2=tk.Label(self.caja_combo, text=self.ingr[i][1],font=("FontAwesome",6),bg=COLOR_CUERPO_PRINCIPAL,)
            self.elemtns2.grid(row=i+1,column=2,sticky='n',padx=10)