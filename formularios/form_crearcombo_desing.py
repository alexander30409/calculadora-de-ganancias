import tkinter as tk
from tkinter import ttk,messagebox
from tkinter import font
from config import  *
from bd.conexion import *

class FormularioCrearComboBD():
    def __init__(self, panel_principal, logo=None):
        # Crear paneles: barra superior
        self.barra_superior = tk.Frame(panel_principal)
        self.barra_superior.pack(side=tk.TOP, fill=tk.X, expand=False) 

        # Crear paneles: barra inferior
        self.barra_inferior = tk.Frame(panel_principal)
        self.barra_inferior.pack(side=tk.BOTTOM, fill='both', expand=True)
        #Titulo barra superior
        self.labelTitulo=tk.Label(self.barra_superior,text='Crear Combos')  
        self.labelTitulo.config(fg='#222d33',font=('Georgia',30))
        self.labelTitulo.pack(side=tk.TOP,fill="both",expand=True)
        self.construirWidgets()
        self.Tabla()

    def construirWidgets(self):
        #caja nombre combo
        self.lnombreCombo=tk.Label(self.barra_inferior,text='Nombre del Combo',font=('Georgia',15))
        self.lnombreCombo.place(relx=0.1,rely=0.08,anchor='sw',width=175)
        self.CnombreCombo=tk.Entry(self.barra_inferior)
        self.CnombreCombo.place(relx=0.1,rely=0.09,width=175,height=20)
        #lista de seleccion de ingredientes
        self.lingredientes=tk.Label(self.barra_inferior,text='Ingredientes',font=('Georgia',15))
        self.lingredientes.place(relx=0.1,rely=0.2,y=-30,width=115,)
        self.lista1=ttk.Combobox(self.barra_inferior,state='readonly',values=ListaBSD('ingredientes','nombre'),width=20,height=8)
        self.lista1.place(relx=0.1,rely=0.2)
        #lista de seleccion de cantidad en gramos del ingrediente
        validacion=self.barra_inferior.register(lambda char: char not in ' .:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvwxyz{|}~!"#$%&*+,)(/-')
        self.lcantidad=tk.Label(self.barra_inferior,text='Cantidad(gr)',font=('Georgia',15))
        self.lcantidad.place(relx=0.1,rely=0.33,y=-30,width=115,)
        self.cajanumerica=ttk.Spinbox(self.barra_inferior,from_=0,to=1000,increment=10,validate='key',validatecommand=(validacion,'%S'))
        self.cajanumerica.place(relx=0.1,rely=0.33,width=70)

        #Boton Enviar 
        btnPasa=tk.Button(self.barra_inferior,text='------->',command=self.obtener_seleccion,fg=COLOR_CUERPO_PRINCIPAL,bg=COLOR_MENU_LATERAL,)
        btnPasa.place(relx=0.1,rely=0.33,y=30,width=175)
        #lista de grupo
        self.lnombreCombo=tk.Label(self.barra_inferior,text='Lista de Ingredientes',font=('Georgia',15))
        self.lnombreCombo.place(x=380,y=10)
        self.lista2=tk.Listbox(self.barra_inferior,selectmode=tk.EXTENDED,font=("FontAwesome", 11))
        self.lista2.place(x=380,y=40 ,height=150,width=230)
        #Boton Quitar 
        btnquitar=tk.Button(self.barra_inferior,text='Quitar',font=("FontAwesome", 12),command=self.quitar,bg=COLOR_MENU_LATERAL,fg=COLOR_CUERPO_PRINCIPAL)
        btnquitar.place(x=380,y=195,width=200)
        #Boton Crear 
        btnquitar=tk.Button(self.barra_inferior,text='Crear Combo',font=("FontAwesome",12),command=self.CrearCombo,bg=COLOR_MENU_LATERAL,fg=COLOR_CUERPO_PRINCIPAL)
        btnquitar.place(x=380,y=225,width=200)
        #Boton Modificar 
        btnmod=tk.Button(self.barra_inferior,text='\u2699',font=("FontAwesome",12),command=self.modificar,bg=COLOR_MENU_LATERAL,fg=COLOR_CUERPO_PRINCIPAL)
        btnmod.place(x=580,y=195,width=30)
        #Boton basurero 
        btnmod=tk.Button(self.barra_inferior,text='\U0001f5d1',font=("FontAwesome",12),command=self.eliminar,bg=COLOR_MENU_LATERAL,fg=COLOR_CUERPO_PRINCIPAL)
        btnmod.place(x=580,y=225,width=30)
    def Tabla(self):

        self.tabla = ttk.Treeview(self.barra_inferior, columns=("#1","#2","#3"),height=10)
        self.tabla.column("#0",width=150)
        self.tabla.column("#1",width=150)
        self.tabla.column("#2",width=150)
        self.tabla.column("#3",width=140)

        self.tabla.heading("#0", text="ID")
        self.tabla.heading("#1", text="Nombre")
        self.tabla.heading("#2", text="Ganancia")
        self.tabla.heading("#3",text="Precio Total")

        for dato in BSD("combos"):
            self.tabla.insert(
                "",tk.END,
                text=dato[0],
                values=dato[1:],
            )
        self.tabla.place(relx=0.1,y=265)


        def handle_click(event):
            if self.tabla.identify_region(event.x, event.y) == "separator":
                return "break"        
        self.tabla.bind('<ButtonRelease-1>', self.selec_valor)
        self.tabla.bind('<Button-1>', handle_click)
    def selec_valor(self,event):
        item=self.tabla.focus()
        self.ID = self.tabla.item(item, 'text')
      # Obtiene los valores del item seleccionado
        self.lista2.delete(0,tk.END)
        self.CnombreCombo.delete(0,tk.END)
        for i,j in Listaingredientesdelcombo(self.ID):
            self.lista2.insert(tk.END,i+' Cantidad:'+str(j))
        self.CnombreCombo.insert(tk.END,FilaBSD('combos',self.ID)[1])
        

    def obtener_seleccion(self):
    # Esto es una tupla con los índices (= las posiciones)
    # de los ítems seleccionados por el usuario
        seleccion=self.lista1.get()
        cant=self.cajanumerica.get()
        if seleccion not in self.lista2.get(0,tk.END):
                self.lista2.insert(tk.END,seleccion+' Cantidad:'+cant)

    def quitar(self):
    # Esto es una tupla con los índices (= las posiciones)
    # de los ítems seleccionados por el usuario
        indices=self.lista2.curselection()
        for i in indices:
            self.lista2.delete(i)
    def CrearCombo(self):
        NombreCombo=self.CnombreCombo.get()
        preciototal=self.preciocosto()+(self.preciocosto()*0.05)
        ganancia=30
        formularara=preciototal/(1-ganancia/100)
        if NombreCombo not in ListaBSD('combos','nombre'):
            InsertcombBSD(NombreCombo,ganancia,preciototal)
        else: return
        for i in self.nombrecantidad():
            InsertcombIngBSD(ID_BSD('combos',NombreCombo),ID_BSD('ingredientes',i[0]),int(i[1]))
        self.Tabla()
        messagebox.showinfo(title='Resultado',message='Total: '+str(formularara))
    def modificar(self):
        ID=self.ID
        NombreCombo=self.CnombreCombo.get()
        preciototal=self.preciocosto()+(self.preciocosto()*0.05)
        ganancia=30
        formularara=preciototal/(1-ganancia/100)
        if NombreCombo not in ListaBSD('combos','nombre') or NombreCombo==FilaBSD('combos',ID)[1]:
            print(1)
            ModificarCombo(NombreCombo,ganancia,preciototal,ID)
            messagebox.showinfo(title='Resultado',message='Total: '+str(formularara))
        else: return
        self.Tabla()
        
    def eliminar(self):
        ID=self.ID
        EliminarCombo(ID)
        self.Tabla()
    def nombrecantidad(self):
        tupla=self.lista2.get(0,tk.END)
        lista=[]
        for i in tupla:
            lista.append(tuple(i.split(' Cantidad:')))
        return lista
    def preciocosto(self):
        lista=[]
        for i in self.nombrecantidad():
            valor=int(i[1])*precioU_BSD(i[0])
            lista.append(valor)
        return sum(lista)
