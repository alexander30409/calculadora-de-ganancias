import tkinter as tk 
from tkinter import font
from config import COLOR_BARRA_SUPERIOR, COLOR_MENU_LATERAL, COLOR_CUERPO_PRINCIPAL, COLOR_MENU_CURSOR_ENCIMA
import util.util_imagenes as util_img 
import util.util_ventana as util_ventana

from formularios.form_modificar_design import FormularioModificarBD
from formularios.form_crearcombo_desing import FormularioCrearComboBD
from formularios.formulario_info_design import FormularioInfoDesign
from formularios.form_indice import FormularioIndice


class FormularioMaestroDesign(tk.Tk):
    def __init__(self):
        super().__init__()
        self.logo = util_img.leer_imagen("./imagenes/logo3.png", (560, 300))
        #self.perfil = util_img.leer_imagen("./imagenes/perfil.png", (100, 100))
        self.config_window()
        self.paneles()
        self.controles_barra_superior()
        self.controles_menu_lateral()
        self.controles_cuerpo()
        

    def config_window(self):
        self.title("Prueba proyecto")
        self.iconbitmap("./imagenes/logo.ico")
        w, h = 1024, 600
        util_ventana.centrar_ventana(self, w, h)

    def paneles(self):
        self.barra_superior = tk.Frame(self, bg= COLOR_BARRA_SUPERIOR, height=50)
        self.barra_superior.pack(side=tk.TOP, fill="both")

        self.menu_lateral = tk.Frame(self, bg= COLOR_MENU_LATERAL, width=150)
        self.menu_lateral.pack(side=tk.LEFT, fill="both", expand=False)

        self.cuerpo_principal = tk.Frame(self, bg= COLOR_CUERPO_PRINCIPAL, width=150)
        self.cuerpo_principal.pack(side=tk.RIGHT, fill="both", expand=True)

    def controles_barra_superior(self):
        font_awesome = font.Font(family='FontAwesome', size=12)

         #etiqueta de titulo
        self.labelTitulo = tk.Label(self.barra_superior, text="Daft Burguer")
        self.labelTitulo.config(fg="#fff", font=("Roboto", 15), bg= COLOR_BARRA_SUPERIOR, pady=10, width=16)
        self.labelTitulo.pack(side=tk.LEFT) 

        # boton del menu lateral

        self.buttonMenuLateral = tk.Button(self.barra_superior, text="\U0001F4D2", font=font_awesome, command=self.toggle_panel,
                                           bd=0, bg=COLOR_BARRA_SUPERIOR, fg="white")
        self.buttonMenuLateral.pack(side=tk.LEFT)

        #Etiqueta de informacion 
        self.labelTitulo = tk.Label(self.barra_superior, text="alexander30409@gmail.com")
        self.labelTitulo.config(fg="#fff", font=("Roboto", 10), bg=COLOR_BARRA_SUPERIOR, padx=10, width=20)
        self.labelTitulo.pack(side=tk.RIGHT)

    def controles_menu_lateral(self):
        ancho_menu = 20
        alto_menu= 2
        font_awesome = font.Font(family="FontAwesome", size=15)

        #etiqueta de perfil 

        self.version = tk.Label(self.menu_lateral, text='Version 1.0', bg=COLOR_MENU_LATERAL, fg="White")
        self.version.pack(side=tk.BOTTOM, pady=10)
        
        #Botones del menu lateral

        self.buttonIndice = tk.Button(self.menu_lateral)        
        self.buttonModificar = tk.Button(self.menu_lateral)        
        self.buttonCombo = tk.Button(self.menu_lateral)
        self.buttonInfo = tk.Button(self.menu_lateral)        
        

        buttons_info = [
            ("Indice", "\u53f8" , self.buttonIndice, self.abrir_indice),
            ("Modificar", "\u270F", self.buttonModificar, self.abrir_panel_modificar),
            ("Crear Combo","\U0001F354", self.buttonCombo, self.abrir_panel_crearcombo),
            ("Informacion", "\U0001F4CE" , self.buttonInfo, self.abrir_panel_info),
        ]

        for text, icon, button, comando in buttons_info:
            self.configurar_boton_menu(button, text, icon, font_awesome, ancho_menu, alto_menu, comando)

    def controles_cuerpo(self):
        # Imagen en el cuerpo principal
        label = tk.Label(self.cuerpo_principal, image=self.logo,
                         bg=COLOR_CUERPO_PRINCIPAL)
        label.place(x=0, y=0, relwidth=1, relheight=1)

    def configurar_boton_menu(self, button, text, icon, font_awesome, ancho_menu, alto_menu, comando):
        button.config(text=f"  {icon}    {text}", anchor="w", font=font_awesome,
                      bd=0, bg=COLOR_MENU_LATERAL, fg="white", width=ancho_menu, height=alto_menu, command= comando)
        button.pack(side=tk.TOP)
        self.bind_hover_events(button)

    def bind_hover_events(self, button):
        # Asociar eventos Enter y Leave con la función dinámica
        button.bind("<Enter>", lambda event: self.on_enter(event, button))
        button.bind("<Leave>", lambda event: self.on_leave(event, button))

    def on_enter(self, event, button):
        # Cambiar estilo al pasar el ratón por encima
        button.config(bg=COLOR_MENU_CURSOR_ENCIMA, fg='white')

    def on_leave(self, event, button):
        # Restaurar estilo al salir el ratón
        button.config(bg=COLOR_MENU_LATERAL, fg='white')

    def toggle_panel(self):
        # Alternar visibilidad del menú lateral
        if self.menu_lateral.winfo_ismapped():
            self.menu_lateral.pack_forget()
        else:
            self.menu_lateral.pack(side=tk.LEFT, fill='y')

#Funcion asociada modificar
    def abrir_panel_modificar(self):
        self.limpiar_panel(self.cuerpo_principal)
        FormularioModificarBD(panel_principal=self.cuerpo_principal)
    def abrir_panel_crearcombo(self):
        self.limpiar_panel(self.cuerpo_principal)
        FormularioCrearComboBD(panel_principal=self.cuerpo_principal)
#Funcion mostrar todo el listado
    def abrir_indice(self):
        self.limpiar_panel(self.cuerpo_principal)
        FormularioIndice(self.cuerpo_principal)

    def limpiar_panel(self, panel):
        for widget in panel.winfo_children():
            widget.destroy()

    def abrir_panel_info(self):
        self.limpiar_panel(self.cuerpo_principal)
        FormularioInfoDesign(self.cuerpo_principal)