import pymysql
basededatos="proyectodb"
def BSD(var):
    conexion = pymysql.connect(host="localhost", user="root", 
                                        passwd="", database=basededatos)
    seleccion="SELECT * FROM "+var
    cursor = conexion.cursor()
    cursor.execute(seleccion)
    datos = cursor.fetchall()
    conexion.close()
    return datos

def InsertingBSD(valores):
    conexion = pymysql.connect(host="localhost", user="root", 
                                        passwd="", database=basededatos)
    cursor = conexion.cursor()
    cursor.execute(
    "INSERT INTO ingredientes VALUES (NULL,%s, %s,%s,%s)",
    valores
    )
    conexion.commit()
    conexion.close()

def ModificarBSD(valores):
    conexion = pymysql.connect(host="localhost", user="root", 
                                        passwd="", database=basededatos)
    cursor = conexion.cursor()
    cursor.execute(
    "UPDATE ingredientes SET nombre=%s,Cantidad=%s,PrecioCosto=%s,PrecioUnidad=%s where id=%s",
    valores
    )
    conexion.commit()
    conexion.close()

def EliminarBSD(valores):
    conexion = pymysql.connect(host="localhost", user="root", 
                                        passwd="", database=basededatos)
    cursor = conexion.cursor()
    cursor.execute(
    "DELETE from ingredientes where id=%s",
    valores
    )
    conexion.commit()
    conexion.close()
def ListaBSD(tabla,var):
    conexion = pymysql.connect(host="localhost", user="root", 
                                        passwd="", database=basededatos)
    seleccionar="SELECT "+var+" FROM "+tabla
    cursor = conexion.cursor()
    cursor.execute(seleccionar)
    datos = cursor.fetchall()
    conexion.close()
    lista=[]
    for i in datos:
        lista.append(i[0])
    return lista
def ID_BSD(tabla,var):
    conexion = pymysql.connect(host="localhost", user="root", 
                                        passwd="", database=basededatos)
    seleccionar="SELECT id FROM "+tabla+" WHERE nombre='"+var+"'"
    cursor = conexion.cursor()
    cursor.execute(seleccionar)
    dato = cursor.fetchone()[0]
    conexion.close()
    return dato
def InsertcombBSD(*args):
    conexion = pymysql.connect(host="localhost", user="root", 
                                        passwd="", database=basededatos)
    cursor = conexion.cursor()
    cursor.execute(
    "INSERT INTO combos VALUES (NULL,%s, %s,%s)",
    args
    )
    conexion.commit()
    conexion.close()
def InsertcombIngBSD(*args):
    conexion = pymysql.connect(host="localhost", user="root", 
                                        passwd="", database=basededatos)
    cursor = conexion.cursor()
    cursor.execute(
    "INSERT INTO comboingredientes VALUES (NULL, %s,%s,%s)",
    args
    )
    conexion.commit()
    conexion.close()
def FilaBSD(tabla,var):
    conexion = pymysql.connect(host="localhost", user="root", 
                                        passwd="", database=basededatos)
    seleccionar="SELECT * FROM "+tabla+" WHERE id="+str(var)
    cursor = conexion.cursor()
    cursor.execute(seleccionar)
    datos = cursor.fetchone()
    conexion.close()
    return datos
def precioU_BSD(var):
    conexion = pymysql.connect(host="localhost", user="root", 
                                        passwd="", database=basededatos)
    seleccionar="SELECT PrecioUnidad FROM ingredientes WHERE nombre='"+var+"'"
    cursor = conexion.cursor()
    cursor.execute(seleccionar)
    dato = cursor.fetchone()[0]
    conexion.close()
    return dato
def ingrIDCantgr(var):
    conexion = pymysql.connect(host="localhost", user="root", 
                                        passwd="", database=basededatos)
    seleccionar="SELECT ingrediente_id,Cangr FROM comboingredientes WHERE combo_id='"+str(var)+"'"
    cursor = conexion.cursor()
    cursor.execute(seleccionar)
    dato = cursor.fetchall()
    conexion.close()
    return dato   
def Listaingredientesdelcombo(var):
    Lista=[]
    for i in ingrIDCantgr(var):
        Lista.append((FilaBSD('ingredientes',i[0])[1],i[1]))
    return Lista
def ModificarCombo(*args):
    conexion = pymysql.connect(host="localhost", user="root", 
                                        passwd="", database=basededatos)
    cursor = conexion.cursor()
    cursor.execute(
    "UPDATE combos SET nombre=%s,Ganancia=%s,`Precio Total`=%s where id=%s",
    args
    )
    conexion.commit()
    conexion.close()
def EliminarCombo(var):
    conexion = pymysql.connect(host="localhost", user="root", 
                                        passwd="", database=basededatos)
    cursor = conexion.cursor()
    cursor.execute(
    "DELETE from combos where id=%s",
    var
    )
    conexion.commit()
    conexion.close()